<?php

namespace App\Controller;

use App\Core\ApiInterface;
use App\Loader\DataLoader;
use App\Sorter\CardsSorter;

class MainController implements ApiInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrderedPath(): array
    {
        $data = (new DataLoader())->getData();

        echo '<pre>';
        var_dump('Unsorted data:', $data);

        return (new CardsSorter())->sortCards($data);
    }
}
