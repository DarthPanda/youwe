<?php

namespace App\Core;

interface ApiInterface
{
    /**
     * @return array
     */
    public function getOrderedPath(): array ;
}