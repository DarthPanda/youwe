<?php

namespace App\Loader;

use App\Mapper\TransportMapper;
use App\Model\BoardingCard;

class DataLoader
{
    /**
     * @return array
     */
    public function getData(): array
    {
        $arrayData = \json_decode(file_get_contents(__DIR__ . '/tickets.json'), true);
        shuffle($arrayData);
        $resultData = [];

        foreach ($arrayData as $key => $cardData) {
            $transport = (new TransportMapper())->map($cardData['transport']);
            $newCard = new BoardingCard($transport, $cardData['from_place'], $cardData['to_place'], $cardData['seat']);
            $newCard->setUid($key);

            $resultData[$newCard->getUid()] = $newCard;
        }

        return $resultData;
    }
}