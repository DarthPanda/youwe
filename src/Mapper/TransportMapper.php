<?php

namespace App\Mapper;

use App\Model\Bus;
use App\Model\Plane;
use App\Model\Train;
use App\Model\Transport;

class TransportMapper
{
    /**
     * @param array $transportData
     * @return Transport|null
     */
    public function map(array $transportData): ?Transport
    {
        $type = $transportData['type'];

        if (!in_array($type, Transport::TRANSPORT_TYPES)) {
            return null;
        }
        $newTransport = '';

        switch ($type) {
            case Transport::BUS:
                $newTransport = new Bus('None');
                break;
            case Transport::PLANE:
                $newTransport = new Plane(
                    $transportData[Transport::NUMBER_KEY],
                    $transportData[Plane::GATE_KEY],
                    $transportData[Plane::BAGGAGE_KEY]
                );
                break;
            case Transport::TRAIN:
                $newTransport = new Train($transportData[Transport::NUMBER_KEY]);
                break;
        }

        return $newTransport;
    }
}