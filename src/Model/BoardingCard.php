<?php

namespace App\Model;

class BoardingCard
{
    /** @var int */
    private $uid;

    /** @var Transport */
    private $transport;

    /** @var string */
    private $fromCity;

    /** @var string */
    private $toCity;

    /** @var string */
    private $seat;

    /**
     * @param Transport $transport
     * @param string $fromCity
     * @param string $toCity
     * @param string $seat
     */
    function __construct(Transport $transport, string $fromCity, string $toCity, string $seat)
    {
        $this->transport = $transport;
        $this->fromCity = $fromCity;
        $this->toCity = $toCity;
        $this->seat = $seat;
    }

    /**
     * @return Transport
     */
    public function getTransport(): Transport
    {
        return $this->transport;
    }

    /**
     * @param Transport $transport
     * @return BoardingCard
     */
    public function setTransport(Transport $transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromCity(): string
    {
        return $this->fromCity;
    }

    /**
     * @param string $fromCity
     * @return BoardingCard
     */
    public function setFromCity(string $fromCity): self
    {
        $this->fromCity = $fromCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getToCity(): string
    {
        return $this->toCity;
    }

    /**
     * @param string $toCity
     * @return BoardingCard
     */
    public function setToCity(string $toCity): self
    {
        $this->toCity = $toCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeat(): string
    {
        return $this->seat;
    }

    /**
     * @param string $seat
     * @return BoardingCard
     */
    public function setSeat(string $seat): self
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return BoardingCard
     */
    public function setUid(int $uid): self
    {
        $this->uid = $uid;

        return $this;
    }
}