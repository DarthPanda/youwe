<?php

namespace App\Model;


class Bus extends Transport
{
    /**
     * @param string $number
     */
    public function __construct(string $number)
    {
        parent::__construct($number);
    }
}