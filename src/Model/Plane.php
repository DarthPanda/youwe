<?php

namespace App\Model;


class Plane extends Transport
{
    public const GATE_KEY = 'gate';
    public const BAGGAGE_KEY = 'baggage';
    /** @var string */
    private $gate;

    /** @var string */
    private $baggage;

    /**
     * @param string $number
     * @param string $gate
     * @param string $baggage
     */
    public function __construct(string $number, string $gate, string $baggage)
    {
        parent::__construct($number);

        $this->gate = $gate;
        $this->baggage = $baggage;
    }

    /**
     * @return string
     */
    public function getGate(): string
    {
        return $this->gate;
    }

    /**
     * @param string $gate
     * @return Plane
     */
    public function setGate(string $gate): self
    {
        $this->gate = $gate;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaggage(): string
    {
        return $this->baggage;
    }

    /**
     * @param string $baggage
     * @return Plane
     */
    public function setBaggage(string $baggage): self
    {
        $this->baggage = $baggage;

        return $this;
    }
}