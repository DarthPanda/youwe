<?php

namespace App\Model;


class Transport
{
    public const NUMBER_KEY = 'number';
    public const BUS = 'airport bus';
    public const TRAIN = 'train';
    public const PLANE = 'plane';
    public const TRANSPORT_TYPES = [
        self::BUS => self::BUS,
        self::TRAIN => self::TRAIN,
        self::PLANE => self::PLANE,
    ];

    /** @var string */
    private $number;

    /**
     * @param string $number
     */
    public function __construct(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Transport
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }
}