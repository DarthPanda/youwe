<?php

namespace App\Sorter;

use App\Model\BoardingCard;

class CardsSorter
{
    /**
     * @param BoardingCard[] $cards
     * @return array
     */
    public function sortCards(array $cards): array
    {
        $first = $this->getFirstCard($cards);
        $result[] = $first;

        return $this->buildResultedArray($first, $cards, $result);
    }

    /**
     * @param BoardingCard[] $cards
     * @return BoardingCard
     */
    private function getFirstCard(array &$cards): BoardingCard
    {
        /** @var BoardingCard $card */
        foreach ($cards as $card) {
            $fromCity = $card->getFromCity();

            $uniqueMatchesCount = count(array_unique(array_map(function ($card) use ($fromCity) {
                /** @var BoardingCard $card */
                return ($card->getToCity() === $fromCity);
            }, $cards)));

            if ($uniqueMatchesCount === 1) {
                unset($cards[$card->getUid()]);
                return $card;
            }
        }
    }


    /**
     * @param BoardingCard $previous
     * @param array $array
     * @param $result
     * @return array
     */
    private function buildResultedArray(BoardingCard $previous, array $array, &$result): array
    {
        /** @var BoardingCard $card */
        foreach ($array as $card) {
            if ($previous->getToCity() === $card->getFromCity()) {
                $result[] = $card;
                unset($array[$card->getUid()]);

                $this->buildResultedArray($card, $array, $result);
            }
        }

        return $result;
    }
}